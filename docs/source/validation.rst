Human validation
================

It is possible to perform Human validation in two different ways.
On one hand, we intend with global
a human generated ground truth in which the identities of all the animals
in a frame are validated. This kind of validation is extremely hard when
dealing with large groups. To verify the goodness of the tracking on the entire
video the individual validation allows to follow a single selected animals.


^^^^^^^^^^^^^^^^^^^^
Generate groundtruth
^^^^^^^^^^^^^^^^^^^^

.. automodule:: generate_groundtruth
  :members:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Compute ground truth statistics
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: compute_groundtruth_statistics
  :members:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Generate individual ground truth
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: generate_individual_groundtruth
  :members:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Compute individual ground truth statistics
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: compute_individual_groundtruth_statistics
  :members:
