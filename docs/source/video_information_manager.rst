Video information manager
=========================

Collects information concerning the video (fps, number of channels, ...), manages the creation of the folders used to save and load both partial and final outputs of the algorithm, and saves general statistics and steps for the different steps of the tracking.

.. automodule:: video
  :members:
